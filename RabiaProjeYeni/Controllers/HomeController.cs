﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RabiaProjeYeni.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Yemek()
        {
            return View();
        }

        public ActionResult Organizasyon()
        {
            return View();
        }

        public ActionResult Giyim()
        {
            return View();
        }

        public ActionResult Nerede()
        {
            return View();
        }
    }
}