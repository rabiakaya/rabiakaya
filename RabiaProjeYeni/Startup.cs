﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RabiaProjeYeni.Startup))]
namespace RabiaProjeYeni
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
